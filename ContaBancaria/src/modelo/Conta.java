
package modelo;

public class Conta {

        protected double saldo;
        protected double deposito;
        protected double retirada;
        
        public Conta(double saldo, double deposito, double retirada){
            this.saldo = saldo;
            this.deposito = deposito;
            this.retirada = retirada;
        }
        
        public void setSaldo(double saldo){
            this.saldo = saldo;
        }
        
        public double getSaldo(){
            return saldo;
        }
        
        public void setDeposito(double deposito){
            this.deposito = deposito;
        }
        
        public double getDeposito(){
            return deposito;
        }
        
        public void setRetirada(double retirada){
            this.retirada = retirada;
        }
        
        public double getRetirada(){
            return retirada;
        }
        
    
    
}
