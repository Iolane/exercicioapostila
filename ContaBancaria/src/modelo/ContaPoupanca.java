package modelo;

public class ContaPoupanca extends Conta{
    
    private double rendimento;
    
    public ContaPoupanca(double saldo, double deposito, double retirada, double rendimento){
        super(saldo, deposito, retirada);
        this.rendimento = rendimento;
    }
    
    public void setRendimento(double rendimento){
        this.rendimento = rendimento;
    }
    
    public double getRendimento(){
        return rendimento;
    }
    
}
