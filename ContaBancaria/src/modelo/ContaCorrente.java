
package modelo;

public class ContaCorrente extends Conta{
    
    private double taxaManutencao;
    
    public ContaCorrente(double saldo, double deposito, double retirada, double taxaManutencao){
        super(saldo, deposito, retirada);
        this.taxaManutencao = taxaManutencao;
    }
    
    public void setTaxaManutencao(double taxaManutencao){
        this.taxaManutencao = taxaManutencao;
    }
    
    public double getTaxaManutencao(){
        return taxaManutencao;
    }
}
